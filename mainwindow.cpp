#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dwgline.h"
#include "dwgcircle.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionLine_triggered()
{
    mCurDrawType=DLine;
}

void MainWindow::on_actionCircle_triggered()
{
    mCurDrawType=DCircle;
}


bool isFirst = true;
QPoint temppt;

void MainWindow::mousePressEvent(QMouseEvent *event)
{ if(mCurDrawType & DLine)
    {
        if(isFirst)
        {
            temppt = event->pos();
            isFirst = false;
        }
        else
        {
            DwgLine *line = new DwgLine;            // 创建并初始化一个直线对象
            line-> SetData (temppt,event->pos());		// 使用SetData函数设置私有成员变量
            mObjVec.append(line);				// append函数用于把对象添加到数组mLines中
            update();						// 该函数用于刷新窗口
            isFirst = true;
        }
    }

    if(mCurDrawType & DCircle)
    {
        if(isFirst)
        {
            temppt = event->pos();
            isFirst = false;
        }
        else
        {
            Dwgcircle *circle = new Dwgcircle;            // 创建并初始化一个直线对象
            circle-> SetData (temppt,event->pos());		// 使用SetData函数设置私有成员变量
            mObjVec.append(circle);				// append函数用于把对象添加到数组mLines中
            update();						// 该函数用于刷新窗口
            isFirst = true;
        }
    }


}

void MainWindow::paintEvent(QPaintEvent *event)
{
    mpainter = new QPainter(this);
    foreach (auto i, mObjVec) {
        i->Draw(mpainter);
    }
    delete mpainter;

}


